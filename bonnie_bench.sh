#!/bin/bash
#Skrypt sluzy do zautomatyzowania benchmarku z uzyciem bonnie++
#Ostatnia modyfikacja 03.04.2019

bonnie_path='/usr/sbin/bonnie++' #sciezka do bonnie++

#nazwa pliku wyjsciowego brana jest jako argument wywolania skrypty
plik_wyjsciowy=$1

#jezezli nie zostal podany arguemnt to zostanie wygenerowana nazwa
plik_wyjsciowy=${plik_wyjsciowy:-plik-log_$(date +%Y-%m-%d_%H:%M)}

>"$plik_wyjsciowy" #wyczyszczenie pliku na wejsciu

bonnie() #Funkcja do wykonania testu
{
  fsname=$1 #nazwa systemu plikow

  nazwa_bench=$2   #nazwa benc:default,64, 128

  #pobranie dostepnej pamieci ram z komendy free
  rozmiar_ram=$(free -m|grep Mem:|awk '{print $2}')

  #obliczenie rozmiaru pliku z wzoru 2*pamiec_ram+200
  rozmiar_pliku=$(((2*$rozmiar_ram)+200))

  #wykonanie testu z uzyciem bonnie++
  $bonnie_path -d /mnt -s "$rozmiar_pliku" -m "SD-$fsname-$nazwa_bench"  -n 10 -b -r "$rozmiar_ram" -u root:root
}

benchmark()
{
  nazwa_bench="$1"
  for i in ext4 xfs btrfs
  do
  	if [ "$i" == "ext4" ]
  	then
  		mkfs.$i /dev/mapper/BACKUP_POOL-HOT_POOL -F
  	else
  		mkfs.$i /dev/mapper/BACKUP_POOL-HOT_POOL -f
  	fi

  	mount /dev/mapper/BACKUP_POOL-HOT_POOL /mnt
  	bonnie "$i" "$nazwa_bench" >> "$plik_wyjsciowy" 2>&1
  	umount /mnt
  done
}

lvremove /dev/BACKUP_POOL/HOT_POOL -f #Usuniecie na poczatku lvm

for i in default 64 128
do
  if [ "$i" == "default" ]
	then
		lvcreate -n HOT_POOL BACKUP_POOL -l 100%FREE -y
    benchmark "$i"
    lvremove /dev/BACKUP_POOL/HOT_POOL -f
	fi

	if [ "$i" == "64" ]
	then
		lvcreate -n HOT_POOL BACKUP_POOL -l 100%FREE -i 8 -I 64 -y
    benchmark "$i"
    lvremove /dev/BACKUP_POOL/HOT_POOL -f
	fi

	if [ "$i" == "128" ]
	then
		lvcreate -n HOT_POOL BACKUP_POOL -l 100%FREE -i 8 -I 128 -y
    benchmark "$i"
    lvremove /dev/BACKUP_POOL/HOT_POOL -f
	fi
done
