#!/bin/bash
#Ostatania modyfikacja 23.08.2019

#sprawdzenie warunkow poczatkowych
sprawdz_warunki()
{
  if [ "$#" -lt 3 ]
  then
    echo "Nie wystarczająca ilosc argumentów uzycie: $0 rodzina(windows|linux|freebsd) nazwa(hostname|jakasNazwa) adres_ip_klienta"
    exit 1
  fi

  if [ "$UID" -ne 0 ]
  then
    echo "nie masz uprwanien superuzytkownika"
    exit 1
  fi
}
inicjuj()
{
  #sprawdzenie war. pocz.
  sprawdz_warunki "$@"

  #okresla rodzine systemu operacyjnego
  rodzina=$1

  #przypisanie 2giego argumentu do zmiennej nazwa
  nazwa=$2

  ##wzorce do zamiany dla pliku klienta
  klient_wzorzec_ip='SWAP_IP'
  klient_wzorzec_nazwa='SWAP_KLIENT'
  klient_wzorzec_haslo='SWAP_PASSWORD'

  ##wzorce do zamiany dla pliku puli
  pula_wzorzec_nazwa='SWAP_NAZWA'
  pula_wzorzec_pamiec='SWAP_PAMIEC'

  ##wzorce do zamiany dla pliku pamiec
  pamiec_wzorzec_nazwa='SWAP_NAZWA'

  #wzorce do zmiany harmonogramu
  harmonogram_wzorzec_nazwa='SWAP_NAZWA'

  #wzorce do zmiany zadania
  zadanie_wzorzec_nazwa='SWAP_NAZWA'
  zadanie_wzorzec_def_zadanie='SWAP_DEF_ZADANIE'
  zadanie_wzorzec_klient='SWAP_KLIENT'

  #separator nazw
  separator_nazwa='_'

  #zamiana na male litery
  rodzina=${rodzina,,}

  #wybor hasel, prefixu w zaleznosci od rodziny systemu
  case $rodzina in
    "linux")
      prefix_nazwa='l'
      klient_haslo=''
      zadanie_def='l_linux-jd'
      ;;
    "windows")
      prefix_nazwa='w'
      klient_haslo=''
      zadanie_def='w_windows-jd'
      ;;
    "freebsd")
      prefix_nazwa='f'
      klient_haslo=''
      zadanie_def='f_freebsd-jd'
      ;;
    *)
      echo "Nie wspierana rodzina systemu podano:$rodzina, a wspierane to windows|linux|freebsd"
      exit 1
  esac

  #Adres IP klienta
  klient_ip=$3

  #katalog z szablonami
  szablon_katalog=${4:-/etc/bareos/skrypty/szablon-director}

  #definicja nazw szablonow
  klient_szablon_nazwa='klient.template'
  pamiec_szablon_nazwa='pamiec.template'
  zadanie_szablon_nazwa='zadanie.template'
  pula_szablon_nazwa='pula.template'
  harmonogram_szablon_nazwa='harmonogram.template'

  #definicja sciezek dla docelowych plikow
  bazowy_zarzadca_katalag='/etc/bareos/bareos-dir.d'
  klient_zarzadca_katalog="$bazowy_zarzadca_katalag/client"
  pamiec_zarzadca_katalog="$bazowy_zarzadca_katalag/storage"
  zadanie_zarzadca_katalog="$bazowy_zarzadca_katalag/job"
  pula_zarzadca_katalog="$bazowy_zarzadca_katalag/pool"
  harmonogram_zarzadca_katalog="$bazowy_zarzadca_katalag/schedule"

  #suffixy nazw
  klient_suffix='fd'
  pamiec_suffix='sd'
  zadanie_suffix='sys'
  harmonogram_suffix='sys'
  pula_suffix='sys'

  #nazwy zasobow
  klient_nazwa_bez="$prefix_nazwa$separator_nazwa$nazwa"
  klient_nazwa="$klient_nazwa_bez-$klient_suffix"
  pamiec_nazwa="$prefix_nazwa$separator_nazwa$nazwa-$pamiec_suffix"
  zadanie_nazwa="$prefix_nazwa$separator_nazwa$nazwa-$zadanie_suffix"
  pula_nazwa="$prefix_nazwa$separator_nazwa$nazwa-$pula_suffix"
  harmonogram_nazwa="$prefix_nazwa$separator_nazwa$nazwa-$harmonogram_suffix"

}

kopiuj_zamien()
{
  #kopiowanie i zamiana dla klienta
  if [ -e "$szablon_katalog/$klient_szablon_nazwa" ]
  then
    cp "$szablon_katalog/$klient_szablon_nazwa" "$klient_zarzadca_katalog/$klient_nazwa.conf"
    chown bareos:bareos "$klient_zarzadca_katalog/$klient_nazwa.conf"
    sed -i "s/$klient_wzorzec_nazwa/$klient_nazwa/;s/$klient_wzorzec_ip/$klient_ip/;s/$klient_wzorzec_haslo/$klient_haslo/"  "$klient_zarzadca_katalog/$klient_nazwa.conf"
  fi


  #kopiowanie i zamiana dla pamieci
  if [ -e "$szablon_katalog/$pamiec_szablon_nazwa" ]
  then
    cp "$szablon_katalog/$pamiec_szablon_nazwa" "$pamiec_zarzadca_katalog/$pamiec_nazwa.conf"
    chown bareos:bareos "$pamiec_zarzadca_katalog/$pamiec_nazwa.conf"
    sed -i "s/$pamiec_wzorzec_nazwa/$pamiec_nazwa/"  "$pamiec_zarzadca_katalog/$pamiec_nazwa.conf"
  fi

  ##tutaj wyjatkuj
  #kopiowanie i zamiana dla zadania
  if [ -e "$szablon_katalog/$zadanie_szablon_nazwa" ]
  then
    cp "$szablon_katalog/$zadanie_szablon_nazwa" "$zadanie_zarzadca_katalog/$zadanie_nazwa.conf"
    chown bareos:bareos "$zadanie_zarzadca_katalog/$zadanie_nazwa.conf"
    sed -i "s/$zadanie_wzorzec_nazwa/$zadanie_nazwa/;s/$zadanie_wzorzec_klient/$klient_nazwa/;s/$zadanie_wzorzec_def_zadanie/$zadanie_def/" "$zadanie_zarzadca_katalog/$zadanie_nazwa.conf"
  fi

  #kopiowanie i zamiana dla puli
  if [ -e "$szablon_katalog/$pula_szablon_nazwa" ]
  then
    cp "$szablon_katalog/$pula_szablon_nazwa" "$pula_zarzadca_katalog/$pula_nazwa.conf"
    chown bareos:bareos "$pula_zarzadca_katalog/$pula_nazwa.conf"
    sed -i "s/$pula_wzorzec_nazwa/$pula_nazwa/;s/$pula_wzorzec_pamiec/$pamiec_nazwa/"  "$pula_zarzadca_katalog/$pula_nazwa.conf"
  fi

  #kopiowanie i zamiana dla harmonogramu
  if [ -e "$szablon_katalog/$harmonogram_szablon_nazwa" ]
  then
    cp "$szablon_katalog/$harmonogram_szablon_nazwa" "$harmonogram_zarzadca_katalog/$harmonogram_nazwa.conf"
    chown bareos:bareos "$harmonogram_zarzadca_katalog/$harmonogram_nazwa.conf"
    sed -i "s/$harmonogram_wzorzec_nazwa/$harmonogram_nazwa/"  "$harmonogram_zarzadca_katalog/$harmonogram_nazwa.conf"
  fi
}
inicjuj "$@" ;kopiuj_zamien
