#!/bin/bash
#Ostatania modyfikacja 01.06.2019

#sprawdzenie warunkow poczatkowych
sprawdz_warunki()
{
  if [ "$#" -lt 2 ]
  then
    echo "Nie wystarczająca ilosc argumentów uzycie: $0 rodzina(windows|linux|freebsd) nazwa(hostname|jakasNazwa) "
    exit 1
  fi

  if [ "$UID" -ne 0 ]
  then
    echo "nie masz uprwanien superuzytkownika"
    exit 1
  fi
}

inicjuj()
{
  #sprawdzenie war. pocz.
  sprawdz_warunki "$@"

  #okresla rodzine systemu operacyjnego
  rodzina=$1

  #przypisanie 2giego argumentu do zmiennej nazwa
  nazwa=$2

  ##wzorce do zamiany dla pliku urzadzenie
  urzadzenie_wzorzec_nazwa='SWAP_NAZWA'

  #separator nazw
  separator_nazwa='_'

  #zamiana na male litery
  rodzina=${rodzina,,}

  #wybor prefixu w zaleznosci od rodziny systemu
  case $rodzina in
    "linux")
      prefix_nazwa='l'
      ;;
    "windows")
      prefix_nazwa='w'
      ;;
    "freebsd")
      prefix_nazwa='f'
      ;;
    *)
      echo "Nie wspierana rodzina systemu podano:$rodzina, a wspierane to windows|linux|freebsd"
      exit 1
  esac


  #katalog z szablonami
  szablon_katalog=${3:-/etc/bareos/skrypty/szablon-storage}

  #definicja nazw szablonow
  urzadzenie_szablon_nazwa='urzadzenie.template'

  #definicja sciezek dla docelowych plikow
  bazowy_storage_katalag='/etc/bareos/bareos-sd.d'
  urzadzenie_storage_katalog="$bazowy_storage_katalag/device"

  #suffixy nazw
  urzadzenie_suffix='sd'

  #nazwy zasobow
  urzadzenie_nazwa="$prefix_nazwa$separator_nazwa$nazwa-$urzadzenie_suffix"

  #bazowa sciezka do katalogu gdzie fizycznie beda zapisane dane
  bazowy_storage_katalog_fs='/STORAGE/main'

  #definicja pelnej sciezka katalogu dla urzadzenia i defacto plikow klienta
  urzadzenie_storage_katalog_fs="$bazowy_storage_katalog_fs/$urzadzenie_nazwa"
}

kopiuj_zamien()
{
  #kopiowanie i zamiana dla urzadzenia
  if [ -e "$szablon_katalog/$urzadzenie_szablon_nazwa" ]
  then
    cp "$szablon_katalog/$urzadzenie_szablon_nazwa" "$urzadzenie_storage_katalog/$urzadzenie_nazwa.conf"
    chown bareos:bareos "$urzadzenie_storage_katalog/$urzadzenie_nazwa.conf"
    sed -i "s/$urzadzenie_wzorzec_nazwa/$urzadzenie_nazwa/"  "$urzadzenie_storage_katalog/$urzadzenie_nazwa.conf"

    #sprawdzenie czy istnieje katalog o nazwie, ktory ma byc utworzony
    if [ ! -d "$urzadzenie_storage_katalog_fs" ]
    then
      mkdir "$urzadzenie_storage_katalog_fs"
      chown bareos:bareos "$urzadzenie_storage_katalog_fs"
    fi
  fi
}
inicjuj "$@" ;kopiuj_zamien
