#!/bin/bash
#Ostatnia modyfikacja 23.08.2019
#Skrypt sluzy do wyodrebienia danych wygenerowanych z programu sar (dane wykorzsystania
#CPU czest. 1 s) z pomiedzy lewego i prawego przedzialu czasu z pliku podanego
#jako pierwszy argument wejsciowy i oblicza  srednia (bez zapisywania)

licznik=0 #zmienna wykorzystywana do nazwy pliku

ilosc=0 #przechowuje ilosc liczb

suma=0 #przechowuje sume liczba

#przechowuje srednia z liczb
srednia=0

plik_tmp=$(tempfile) #sciezka do pliku tymczasowego

for i in  "18:39:07;19:03:44" "19:05:00;19:21:06" "19:27:00;19:46:55" "19:52:00;20:09:56" "20:15:00;20:32:13" "20:40:00;20:53:48" "21:00:00;21:16:56"
do

  ((++licznik)) #zwiekszenie licznika o 1

  lewy=$(echo "$i"|cut -d ";" -f1) #lewy przedzial czasu

  prawy=$(echo "$i"|cut -d ";" -f2) #prawy przedzial czasu

  #wyodrebnienie wartosci z pomiedzy lini na podstawie lewy i prawego przedzialu
  awk /$lewy/,/$prawy/ $1 |awk '{print $3+$5}' > "$2-zadanie$licznik.txt"

  while read line   #obliczenie sumy i ilosci
  do
    ((suma+=line))
    ((++ilosc))
  done<"$2-zadanie$licznik.txt"

  srednia=$(echo "$suma/$ilosc"|bc -l) #obliczenie sredniej

  echo "Obliczona: suma=$suma;ilosc=$ilosc;srednia=$srednia"

  #wyzerowanie
  ilosc=0
  suma=0
  srednia=0

done

rm $plik_tmp #usuniecie pliku tmp
