#!/bin/bash
#Ostatnia modyfikacja 03.07.2019
#Skrypt do instalacji klienta
#Dodanie rear
raportuj()
{
  #1szy argument to wiadomosc
  wiadomosc=$1

  echo "$wiadomosc"
}

sprawdz_warunki()
{
  #sprawdzenie czy sa min 2 arg wej
  if [ "$#" -lt 1 ]
  then
    raportuj "Nie wystarczająca ilosc argumentów uzycie: $0 ip [nazwa][katalog_szablonu]"
    exit 1
  fi

  if [ "$UID" -ne 0 ]
  then
    raportuj "nie masz uprwanien superuzytkownika"
    exit 1
  fi


  #wykonanie polecenia ping na adres pod zmienna ping_addr
  ping -c 10 "$ping_addr"; status=$?

  #sprawdzenie czy status wykonanie polecenia ping na wskazany adres jest rozny od 0
  if [ "$status" -ne 0 ]
  then
    raportuj "adres $ping_addr jest osiagalny, koncze swoje dzialanie"
    exit 1
  fi
}

inicjuj()
{
  #adres do pingowania
  ping_addr='google.com'

  #sprawdzenie war pocz
  sprawdz_warunki "$@"

  #Okresla wersje systemu np 9
  wersja_systemu=$(grep -E 'VERSION_ID=' /etc/os-release |cut -d '"' -f2)
  wersja_systemu+='.0'

  #okresla nazwe dystrybucji np. Debian
  nazwa_dystrybucji=$(grep -E '^ID=' /etc/os-release |cut -d '=' -f2)
  nazwa_dystrybucji=${nazwa_dystrybucji^}

  #deklaracja wersji Bareos do pobrania
  wersja_bareos='18.2'

  #zbudowanie nazwy np Debian_9.0
  nazwa_systemu="$nazwa_dystrybucji"
  nazwa_systemu+='_'
  nazwa_systemu+="$wersja_systemu"

  #adres ip klienta
  klient_ip=$1

  #nazwa klienta $2 w przeciwnym razie $HOSTNAME
  klient_nazwa="${2:-$HOSTNAME}"

  #katalog z szablonami
  szablon_katalog=${3:-/etc/bareos/szablon-client}

  #adres url z repozytorium bareos
  adres_url="http://download.bareos.org/bareos/release/$wersja_bareos/$nazwa_systemu"

  ##wzorce do zamiany dla pliku klienta
  klient_wzorzec_nazwa='SWAP_NAZWA'
  klient_wzorzec_ip='SWAP_IP'

  #nazwa pliku konf z zarzadca
  zarzadca_nazwa='bareos-dir'

  #definicja nazw szablonow
  klient_szablon_nazwa='klient.template'
  zarzadca_szablon_nazwa='zarzadca.template'

  #definicja sciezek dla docelowych plikow
  bazowy_klient_katalog='/etc/bareos/bareos-fd.d'
  klient_katalog="$bazowy_klient_katalog/client"
  klient_zarzadca_katalog="$bazowy_klient_katalog/director"
}

kopiuj_zamien()
{
  #kopiowanie i zamiana dla pliku klienta po stronie File Dameona
  if [ -e "$szablon_katalog/$klient_szablon_nazwa" ]
  then
    #usuniecie istniejacych plikow conf
    rm "$klient_katalog/"*.conf
    cp "$szablon_katalog/$klient_szablon_nazwa" "$klient_katalog/$klient_nazwa.conf"
    chown bareos:bareos "$klient_katalog/$klient_nazwa.conf"
    sed -i "s/$klient_wzorzec_nazwa/$klient_nazwa/;s/$klient_wzorzec_ip/$klient_ip/" "$klient_katalog/$klient_nazwa.conf"
  fi

  #kopiowanie i zamiana dla pliku director po stronie File Dameona
  if [ -e "$szablon_katalog/$zarzadca_szablon_nazwa" ]
  then
    #usuniecie istniejacych plikow konf
    rm "$klient_zarzadca_katalog/"*.conf
    cp "$szablon_katalog/$zarzadca_szablon_nazwa" "$klient_zarzadca_katalog/$zarzadca_nazwa.conf"
    chown bareos:bareos "$klient_zarzadca_katalog/$zarzadca_nazwa.conf"
  fi

  #przeladowanie konf
  service bareos-fd restart
}

debian_klient()
{
  #odpytanie menadzera pakietow czy jest zainstalowany pakiet bareos-filedaemon
  czy_pakiet=$(dpkg -l "bareos-filedaemon"|grep -E '^ii' |awk '{print $2}')

  #Jezeli pakiet zainstalowany wyjsc z funkcji w przeciwny razie zainstalowanie
  if [ "$czy_pakiet" == "bareos-filedaemon" ]
  then
    raportuj "Klient juz jest zainstalowany"
    return 0
  else

    #dodanie repozytorium bareos
    echo "deb $adres_url /" >/etc/apt/sources.list.d/bareos.list

    #pobranie klucza gpg
    wget -q $adres_url/Release.key -O- | apt-key add -

    #odswiezenie men. pakietow
    apt update

    #instalacji pakietu Bareos
    apt install bareos-filedaemon -y
  fi

}



inicjuj "$@"

case "$nazwa_dystrybucji" in
  "Debian")
    debian_klient
    kopiuj_zamien
    ;;
  *)
  raportuj "Nie obslugiwany system, podano $nazwa_dystrybucji, obslugiwane to Debian"
  exit 1
esac
