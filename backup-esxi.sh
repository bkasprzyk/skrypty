#!/bin/bash
#Ostatnia modyfikacja 02.07.2019
raportuj()
{
  #pierwszy argument stanowi wiadomosc, ktora bedzie wpisana do zmiennej plik_log
  wiadomosc=$1

  #okresla czy wiadomosc ma byc wyslana rowniez na standardowe wyjscie
  czy_stdout=$2

  data=$(date +%Y-%m-%d" "%H:%M:%S) #data w formacie YYYY-M-DD H:M:S

  plik_log='/var/log/backup-esxi.log' #sciezka do pliku_log

  echo "$data $wiadomosc" >> "$plik_log" #zapisanie wiadomosci do zmiennej plik_log

  if [ "$czy_stdout" == "tak" ]
  then
    echo "$data $wiadomosc" #wyswietlenie wieadomosci na standardowe wyjscie
  fi
}
sprawdz_warunki()
{
  inicjuj "$@"

  #jezeli liczba argumentow wejsciowych jest <1 to zakoncz swoje dzialanie
  if [ $# -lt 4 ]
  then
    raportuj "Nie wystarczajaca liczba argumentow wejsciowych podano:$#, uzycie: $0 akcja(kopiuj|przywroc) ip uzytkownik haslo [port]" "tak"
    exit 1
  fi

  #odpytanie menadzera pakietow czy jest zainstalowany pakiet sshpass
  czy_pakiet=$(dpkg -l "sshpass"|grep -E '^ii' |awk '{print $2}')

  if [ "$czy_pakiet" != "sshpass" ] #sprawdzenie czy jest zainstalowany pakiet sshpass
  then
    raportuj "Brakuje pakietu sshpass, zainstaluj go tym poleceniem" "tak"
    raportuj "sudo apt install sshpass" "tak"
    raportuj "koncze swoje dzialanie" "tak"
    exit 1
  fi
}
esxi-skrypt()
{
cat > "$plik_tmp"<<EOF
#!/bin/sh
#Ostatnia modyfikacja 02.07.2019

vm_id=\$(vim-cmd vmsvc/getallvms|grep -E '^[0-9]'|awk '{print \$1}')

for i in \$vm_id
do
  status=\$(vim-cmd vmsvc/power.getstate "\$i" |grep -v 'info')
  if [ "\$status" != "Powered off" ]
  then
    exit 1
  fi
done
EOF
}

inicjuj()
{
  akcja=$1 #akcja do wykonania

  ip=$2 #adres ip esxi

  uzytkownik=$3 #nazwa uzytkownika esxi

  haslo=$4 #haslo do esxi

  port=${5:-22} #numer portu do polaczenia przez ssh, domyslnie 22

  archiwum_nazwa_suffix='tgz'
  archiwum_nazwa_prefix='configBundle'
  przywroc_archiwum_nazwa="$archiwum_nazwa_prefix.$archiwum_nazwa_suffix"

  sshpass_bin=$(which sshpass) #sciezka do wykonywwalnego pliku sshpass

  ssh_bin=$(which ssh) #sciezka do wyko pliku ssh

  plik_tmp=$(tempfile) #plik tymczasowy

  esxi-skrypt #wypelnienie zmiennej plik_tmp
}

sprzataj ()
{
  if [ -e "/tmp/$archiwum_nazwa" ] && [ ! -z "$archiwum_nazwa" ]
  then
    rm "/tmp/$archiwum_nazwa"
  fi

  if [ -e "$plik_tmp" ]
  then
    rm "$plik_tmp" #usuniecie pliku_tmp
  fi

  if [ -e "/tmp/$przywroc_archiwum_nazwa" ] && [ ! -z "$przywroc_archiwum_nazwa" ]
  then

    rm "/tmp/$przywroc_archiwum_nazwa" #usuniecie /tmp/$przywroc_archiwum_nazwa
  fi
}

wykonaj_cmd()
{
  cmd=$1 #komenda do wykonania na esxi

  ignoruj=$2 #zmienna, ktora okresla czy bedzie zignorowanie sprawdzenie statusu

  "$sshpass_bin" -p"$haslo" "$ssh_bin" -o StrictHostKeyChecking=no -p "$port" "$uzytkownik"@"$ip" "$cmd";status=$?

  if [ "$status" -ne 0 ] && [ "$ignoruj" != "tak" ] #sprawdzenie status wykonania komendy $cmd
  then
    raportuj "Wykonanie komendy $cmd, zakonczylo sie statusem:$status, oczekiwano 0" "tak"
    exit 1
  fi
}
kopiuj()
{
  wykonaj_cmd "vim-cmd hostsvc/firmware/sync_config" #sycnhronizacja firmware

  #utworzenie kopii zapasowej konfiguracji esxi
  wykonaj_cmd "vim-cmd hostsvc/firmware/backup_config >/dev/null 2>&1"

  esxi_hostname=$(wykonaj_cmd "hostname") #pobranie nazwy host z esxi

  #nazwa archiwum z kopia zapasowa konfiguracji esxi
  archiwum_nazwa="$archiwum_nazwa_prefix-$esxi_hostname.$archiwum_nazwa_suffix"

  #zmienna, ktora przechowuje
  archiwum_sciezka_abs=$(wykonaj_cmd "find /scratch/downloads -name $archiwum_nazwa"|head -n 1)

  #zmienna ktora przechowuje file hash kopii zapasowej(po stronie esxi)
  esxi_archiwum_hash=$(wykonaj_cmd "md5sum $archiwum_sciezka_abs"|awk '{print $1}')

  #pobranie archiwum przez ssh za pomoca strumienia i zapisanie na serwerze
  wykonaj_cmd "cat $archiwum_sciezka_abs" > "/tmp/$archiwum_nazwa"

  #zmienna ktora przechowuje file hash kopii zapasowej(po stronie serwera)
  serwer_archiwum_hash=$(md5sum "/tmp/$archiwum_nazwa"|awk '{print $1}')

  #porownanie czy file hash po stronie serwera jest rowny file hash po stronie esxi
  if [ "$esxi_archiwum_hash" != "$serwer_archiwum_hash" ]
  then
    raportuj "File hash sa nie rowne, po stronie esx:=$esxi_archiwum_hash;po stronie serwera:=$serwer_archiwum_hash;" "tak"
    exit 1
  fi

  #wyswietlenie na standardowe wyjscie zawartosci archiwum
  cat "/tmp/$archiwum_nazwa"
}
przywroc()
{
  #zapisanie strumienia ze standardowego wejscia do arhiwum .tgz na serwerze
  cat > "/tmp/$przywroc_archiwum_nazwa"

  #zmienna ktora przechowuje file hash kopii zapasowej(po stronie serwera)
  serwer_archiwum_hash=$(md5sum "/tmp/$przywroc_archiwum_nazwa"|awk '{print $1}')

  #zapisanie strumienia ze standardowego wejscia do arhiwum .tgz na esxi
  wykonaj_cmd "cat >/tmp/$przywroc_archiwum_nazwa" < "/tmp/$przywroc_archiwum_nazwa"

  #zmienna ktora przechowuje file hash kopii zapasowej(po stronie esxi)
  esxi_archiwum_hash=$(wykonaj_cmd "md5sum /tmp/$przywroc_archiwum_nazwa"|awk '{print $1}')

  #porownanie czy file hash po stronie serwera jest rowny file hash po stronie esxi
  if [ "$esxi_archiwum_hash" != "$serwer_archiwum_hash" ]
  then
    raportuj "File hash sa nie rowne, po stronie esx:=$esxi_archiwum_hash;po stronie serwera:=$serwer_archiwum_hash;" "tak"
    exit 1
  fi

  #umieszczenie skryptu na serwerze
  wykonaj_cmd "cat >/tmp/wykonaj.sh" < "$plik_tmp"

  wykonaj_cmd "chmod u+x /tmp/wykonaj.sh" >/dev/null 2>&1 #nadanie uprawnien do wykonania pliku

  #sprawdzenie czy maszyna jakakolwiek wirtualna jest wlaczona
  czy_vm=$(wykonaj_cmd "/tmp/wykonaj.sh;status=$?;echo $status")

  if [ "$czy_vm" -ne 0 ]
  then
    raportuj "Na hoscie: $ip, co najmniej jedna maszynie nie jest w stanie Powered Off, koncze swoje dzialanie" "tak"
    exit 1
  fi

  #przelaczenie esxi w tryb maintenance
  wykonaj_cmd "vim-cmd hostsvc/maintenance_mode_enter >/dev/null 2>&1" "tak"

  #pobranie statusu maintenance
  czy_maintenance=$(wykonaj_cmd "vim-cmd /hostsvc/hostsummary | grep inMaintenanceMode|cut -d '=' -f2"|sed 's/,//gI;s/ //gI')

  if [ "$czy_maintenance" == "false" ] #sprawdzenie jaki status zwrocony
  then
    raportuj "Serwer esxi:$ip, nie jest w trybie maintenance, oczekiwano: true, otrzymano:$czy_maintenance, koncze swoje dzialanie" "tak"
    exit 1
  fi

  #przywrocenie konfiguracji esxi
  wykonaj_cmd "vim-cmd hostsvc/firmware/restore_config /tmp/$przywroc_archiwum_nazwa" "tak"
}
sprawdz_warunki "$@" #sprawdzenie warunkow poczatkowych oraz wywolanie funkcji inicjuj

case "$akcja" in
  "kopiuj")
    kopiuj
    ;;
  "przywroc")
    cat | przywroc
    ;;
  *)
  raportuj "Obslugiwane akcje to: przywroc|kopiuj, podano:$akcja, koncze wiec" "tak"
  exit 1
esac
sprzataj #posprzatanie
