#!/bin/bash
#Skrypt sluzy do zautomatyzowania benchmarku z uzyciem bonnie++
#Ostatnia modyfikacja 06.04.2019

###Zmienne
plik_wyjsciowy=$1
plik_wyjsciowy=${plik_wyjsciowy:-plik-log_$(date +%Y-%m-%d_%H:%M)}
>"$plik_wyjsciowy"
dysk=/dev/md2
sciezka='/mnt'

do_test()
{
  tytul=$1   #nazwa maszyny

  sciezka=$2 #sciezka do zapisu/odczytu
  #pobranie dostepnej pamieci ram z komendy free
  rozmiar_ram=$(free -m|grep Mem:|awk '{print $2}')

  #obliczenie rozmiaru pliku z wzoru 2*pamiec_ram+200
  rozmiar_pliku=$(((2*$rozmiar_ram)+200))

  /usr/sbin/bonnie++ -d "$sciezka" -s "$rozmiar_pliku" -m "$tytul"  -n 10 -b -r "$rozmiar_ram" -u root:root
}

for i in ext4 xfs btrfs
do
	if [ "$i" == "ext4" ]
	then
		mkfs.$i $dysk -F
	else
		mkfs.$i $dysk -f
	fi
	mount "$dysk" "$sciezka"
	do_test "laptop-$i" "$sciezka" >>"$plik_wyjsciowy" 2>&1
	umount "$sciezka"
done
