#!/bin/bash
#Data ostatniej modyfikacji 01.07.2019

raportuj()
{
  #pierwszy argument stanowi wiadomosc, ktora bedzie wpisana do zmiennej plik_log
  wiadomosc=$1

  data=$(date +%Y-%m-%d" "%H:%M:%S) #data w formacie YYYY-M-DD H:M:S

  echo "$data $wiadomosc" >> "$plik_log" #zapisanie wiadomosci do zmiennej plik_log
}

inicjuj()
{
  plik_log='/var/log/bareos/bootstrap.log' #sciezka do pliku z logiem

  klient_nazwa=$1 #pierwszy argument stanowi nazwe klienta

  zadanie_nazwa=$2 #zmienna, ktora przechowuje nazwe zadania

  zadanie_poziom=$3   #zmienna, ktora przechowuje poziom zadania

  zadanie_poziom=${zadanie_poziom/ /} #usuniecie spacji

  rok=$(date +%Y)   #zmienna, ktora przechowuje rok

  #zmienna przechowująca date w formacie YYYY-mm-dd_H:M:S
  data=$(date +%Y-%m-%d_%H:%M:%S)

  katalog_bootstrap=${4:-/media/bootstrap/main} #sciezka do katalogu kopiami

  nazwa_bootstrap_suffix='bsr' #suffix nazwy pliku bootstrap

  #Nazwa pliku bootstrap
  nazwa_bootstrap="$klient_nazwa-$zadanie_nazwa.$nazwa_bootstrap_suffix"

  #Nazwa archiwalnego pliku bootstrap
  nazwa_bootstrap_arch="$klient_nazwa-$zadanie_nazwa-$data.$nazwa_bootstrap_suffix"

  #zamiana -fd na -sd z konca zmiennej klient_nazwa
  klient_katalog=${klient_nazwa/%-fd/-sd}

  #absolutna sciezka do katalogu z bootsrapem dla danego klienta
  klient_katalog_abs="$katalog_bootstrap/$klient_katalog"

  katalog_archiwum_nazwa="archiwum" #nazwa katalogu z archiwum

  #absolutna sciezka do katalogu z archiwum
  katalog_archiwum_abs="$klient_katalog_abs/$katalog_archiwum_nazwa/$rok/$zadanie_nazwa"

  #absolutna sciezka do pliku bootstrap
  plik_bootstrap_abs="$klient_katalog_abs/$nazwa_bootstrap"

  #absolutna sciezka do archiwalnego pliku bootstrap
  plik_bootstrap_arch_abs="$katalog_archiwum_abs/$nazwa_bootstrap_arch"
}

sprawdz_warunki()
{
  inicjuj "$@"
  if [ ! -e "$plik_log" ] #jezeli nie ma pliku z logiem
  then
    echo "Brakuje pliku z logiem"
    touch "$plik_log"
  fi

  #jezeli liczba argumentow wejsciowych jest <1 to zakoncz swoje dzialanie
  if [ $# -lt 3 ]
  then
    raportuj "Nie wystarczajaca liczba argumentow wejsciowych podano:$#, uzycie: $0 nazwa_klienta zadanie_poziom"
    exit 1
  fi
}

zapisz_bootstrap()
{
  #sprawdzenie czy sciezka istnieje pod zmienna katalog_klient_abs
  if [ -e "$klient_katalog_abs" ]
  then
    if [ "$zadanie_poziom" == "Full" ] || [ "$zadanie_poziom" == "VirtualFull" ]
    then
      #sprawdzenie czy istnieje archiwalny katalog
      if [ ! -d "$katalog_archiwum_abs" ]
      then
        mkdir -p "$katalog_archiwum_abs" #utworzenie archiwalnego katalogu
      fi

      #zrobienie archiwalnej kopii pliku bootstrap, jezeli jest aktualny plik bootstrap
      if [ -e "$plik_bootstrap_abs" ]
      then
        cp "$plik_bootstrap_abs" "$plik_bootstrap_arch_abs"
      fi
        #nadpisanie pliku z aktualnym plikiem bootstrap
        cat >"$plik_bootstrap_abs"
    else
      cat >> "$plik_bootstrap_abs"
    fi
  else
    raportuj "Brakuje sciezki: $klient_katalog_abs , koncze swoje dzialanie"
    exit 1
  fi
}
sprawdz_warunki "$@";zapisz_bootstrap ###wywolanie
