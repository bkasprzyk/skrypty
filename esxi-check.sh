#!/bin/sh
#Ostatnia modyfikacja 02.07.2019
#Skrypt sprawdza czy maszyny wirtualne sa wylaczone

vm_id=$(vim-cmd vmsvc/getallvms|grep -E '^[0-9]'|awk '{print $1}')

for i in $vm_id
do
  status=$(vim-cmd vmsvc/power.getstate "$i" |grep -v 'info')
  if [ "$status" != "Powered off" ]
  then
    exit 1
  fi
done
