#!/bin/bash
#Skrypt sluzy do zautomatyzowania benchmarku z uzyciem zcav
#Ostatnia modyfikacja 03.04.2019

suffix='.zcav'
prefix_read='BACKUP_POOL-read'
prefix_write='BACKUP_POOL-write'

#Usuniecie na poczatku lvm
lvremove /dev/BACKUP_POOL/HOT_POOL -f

for i in default 64 128
do

	if [ "$i" == "default" ]
	then
		lvcreate -n HOT_POOL BACKUP_POOL -l 100%FREE
	fi

	if [ "$i" == "64" ]
	then
		lvcreate -n HOT_POOL BACKUP_POOL -l 100%FREE -i 8 -I 64 -y
	fi

	if [ "$i" == "128" ]
	then
		lvcreate -n HOT_POOL BACKUP_POOL -l 100%FREE -i 8 -I 128 -y
	fi

  #Wykonanie testu na odczyt
  zcav /dev/mapper/BACKUP_POOL-HOT_POOL >> "$prefix_read-$i$suffix"

  #Wykonanie testu na zapis
  zcav /dev/mapper/BACKUP_POOL-HOT_POOL -w >> "$prefix_write-$i$suffix"

  #Usuniecie lvm
  lvremove /dev/BACKUP_POOL/HOT_POOL -f

done
